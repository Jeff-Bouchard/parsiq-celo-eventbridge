module.exports = {
    devServer: {
        proxy: {
            '/socket.io': 'http://localhost:3001'
        }
    }
};