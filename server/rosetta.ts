import { mkPostFn, mkJsonFn, mkAPI } from "./rpc";
import { NetworkIdentifier, RosettaAPI, BlockIdentifier } from "./rosetta-types";
import * as sqlite3 from "sqlite3";
import Web3 from "web3";
import net from "net";
import { cUSD_ABI } from "./cUSD";


export type CeloHistory = {
	[index: number]: any
}

export type CeloInitContext = {
	rosetta: RosettaAPI,
	network_identifier: NetworkIdentifier,
	currentBlock: number,
	history: CeloHistory 
}

export const initCelo = async (GETH_RPC: string, ROSETTA_RPC: string, CELO_DATA_DIR: string) => {

	
	const rosetta_db_path = CELO_DATA_DIR + "/rosetta.db";
	const geth_ipc_path = CELO_DATA_DIR + "/geth.ipc";

	const web3 = new Web3(geth_ipc_path, net as any);

	var db = new sqlite3.Database(rosetta_db_path);
	let registryRows = await new Promise((resolve, reject) => 
		db.all("SELECT contract, hex(address) as addr FROM registry",(err, res) => err ? reject(err) : resolve(res))
	) as {contract:string, addr:string}[];

	let registry = {} as {[contract: string]: string};
	let invRegistry = {} as {[addr: string]: string};
	
	for (let c of registryRows) {
		registry[c.contract] = c.addr;
		invRegistry[c.addr] = c.contract;
	}

	const cUSDRegistryName = "StableToken";
	const cUSDAddress = registry[cUSDRegistryName];

	const cGLDRegistryName = "GoldToken";
	const cGLDAddress = registry[cGLDRegistryName];

	console.log(registry);



	const geth = mkJsonFn(mkPostFn(GETH_RPC), () => 1);
	const rosetta = mkAPI(mkPostFn(ROSETTA_RPC), RosettaAPI) as RosettaAPI;

	const net_version = await geth("net_version");
	const network_identifiers = (await rosetta.networkList({})).network_identifiers;
	const network_identifier = network_identifiers.filter(ni => ni.network === net_version.result)[0];
	if (!network_identifier) throw new Error("Unknown Network");
	const currentBlock = (await rosetta.networkStatus({network_identifier})).current_block_identifier.index;
	const history = {} as CeloHistory
	let cUSD = new web3.eth.Contract(cUSD_ABI, cUSDAddress)
	let cUSDCurrency = {
		symbol: await cUSD.methods.symbol().call(),
		decimals: (await cUSD.methods.decimals().call())|0
	}

	console.log(cUSDCurrency)

	return {rosetta, network_identifier, currentBlock, history, web3, cUSD, cUSDCurrency}
}

export const buildHistory = async (ctx: CeloInitContext, traceBlock: (block_identifier: BlockIdentifier) => any) => {
	const {rosetta, network_identifier, currentBlock, history} = ctx;
	const ONE_DAY = (60 * 60 * 24 / 5) // Average block per 5 seconds
	for (let index = currentBlock - ONE_DAY; index < currentBlock; index++) {
		try {
			await traceBlock({index})
		} catch (e) {
			console.log("ERROR");
			console.error(e);
		}
	}
}




