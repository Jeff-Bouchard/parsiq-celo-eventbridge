import React, { useEffect, useState } from 'react';
import { Card, CardContent, Chip, Avatar, CardHeader, IconButton } from '@material-ui/core';
import './App.css';
import io from 'socket.io-client';

const socket = io();

const ICONS = {
	fee: "🍰",
	transfer: "⛴",
	createAccount: "👶",
	lockGold: "🔑",
	unlockGold: "🔓",
	relockGold: "🔐",
	withdrawGold: "🏅",
	vote: "🗳",
	activateVotes: "🔔",
	revokePendingVotes: "🔕",
	revokeActiveVotes: "📣",
	slash: "📏",
  epochRewards: "🎖",
  mint: "💵",
  burn: "🔥"
} as {[type: string]: string}

export const CURRENCIES = {
	cGLD: "🏆",
	cUSD: "💵"
}

type Block = {
  id: {
    index?: number,
    hash?: string
  }
}

type BlockState = {
  block: Block,
  txs: any[],
  idx: any
}

const MAX_TX_COUNT = 256;
const Status = ({value}:{value: string}) => <Chip label={value} size="small"/>
const Account = ({account}:{account: {address:string, sub_account?:{address: string}}}) => <div><div>{account.address}</div>{account.sub_account ? <div>{account.sub_account?.address}</div> : ""}</div>
const Amount = ({amount}:{amount:{value:string,currency:{symbol:string, decimals: number}}}) => {
  let sign = "+";
  let value = amount.value;
  if (value.charAt(0) === "-") {
    value = value.substr(1);
    sign = "-";
  }
  value = value.padStart(amount.currency.decimals+1,"0");
  let fraction = value.substr(-amount.currency.decimals).replace(/0+$/g,"")||"0";
  let whole = value.substring(0, value.length - amount.currency.decimals)
  return <><span className="amount">{`${sign}${whole}.${fraction} ${amount.currency.symbol}`}</span></>;
}

const defaultState = {block: {id:{}}, txs:[], idx: {}} as BlockState

function App() {

  const [state, setBlockState] = useState(defaultState);
  const [version, setVersion] = useState(0);

  function applyHistory(state: BlockState, history: any = {}, block?: Block) {
    state = {...state}
    if (block) {
      state.block = block;
    }
    Object.assign(state.idx, history);
    let txProcessed = 0;
    let blockIndex = state.block.id.index || 0;
    let txs = []
    while(txs.length < MAX_TX_COUNT && blockIndex > 0) {
      let block = state.idx[blockIndex--];
      if (!block) continue;
      txs.push(...Object.values(block.tx).reverse());
    }
    state.txs = txs;
    console.log(txs)
    return state;
  }

  useEffect(() => {
    socket.on('message', (block: Block) => {
      setBlockState(applyHistory(state, {[`${block.id.index}`]:block}, block));
    })
    socket.on('history', (history: any) => {
      setBlockState(applyHistory(state, history));
    })
  },[]);
  useEffect(() => {
    socket.on('version', (serverVersion: number) => {
      if (version > 0 && serverVersion !== version) {
        window.location.reload();
      }
      setVersion(serverVersion);
    })
  },[version]);


  return (
    <div className="App">
      <header className="App-header">
        <h1>Last block: {state.block.id.index ?? '######'}</h1>
        <small>{state.block.id.hash ?? 'retrieving hash...'}</small>
        <p>
          PARSIQ-Celo Event Bridge Playground
        </p>
        <br/>
        More about
        <p className="links">
        <a
          className="App-link"
          href="https://parsiq.net"
          target="_blank"
          rel="noopener noreferrer"
        >PARSIQ</a>|<a
        className="App-link"
        href="https://parsiq.net"
        target="_blank"
        rel="noopener noreferrer"
      >CELO</a>
      </p>
  <h3>Last {MAX_TX_COUNT} tx of last 24h decoded (live):</h3>
        <ul className="tx-list">{state.txs.map(tx => 
          <li className="tx" key={tx.transaction_identifier.hash}>
            <Card>
            <CardHeader
        avatar={
          <Avatar aria-label="recipe">
            Tx
          </Avatar>
        }
        title={tx.transaction_identifier.hash}
        subheader={`#${tx.block_identifier.index}`}
      />
              <CardContent>
                <ol>{tx.operations.map((op:any) => 
                  <li key={op.operation_identifier.index}>
                    <Account account={op.account}/>
                      <Chip avatar={<Avatar>{ICONS[`${op.type}`]||"?"}</Avatar>} size="small" label={op.type} />
                    {op.amount ? <Amount amount={op.amount}/>:""}
                    <Status value={op.status}/>
                    {op.related_operations && JSON.stringify(op.related_operations.map((x:{index: number}) => x.index + 1))}
                  </li>)}
                </ol>
              </CardContent>
            </Card>
          </li>)}
        </ul>

      </header>
      
    </div>
  );
}

export default App;
